
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag
from nltk.corpus import stopwords
from nltk import classify, DecisionTreeClassifier, NaiveBayesClassifier

import re, string, random

from tkinter import *

class SentimentWordNet:

    sen_dict = dict()
    sent_size = 40

    def __init__(self):
        f = open("IMDB Dataset.csv", encoding="utf8")
        (positive_articles, negative_articles) = self.loadCorpus(f)

        positive_tokens_dirty = [ nltk.tokenize.casual_tokenize(x) for x in positive_articles ]
        negative_tokens_dirty = [ nltk.tokenize.casual_tokenize(x) for x in negative_articles ]

        positive_tokens = []
        negative_tokens = []
        stop_words = stopwords.words('english')

        for token in positive_tokens_dirty:
            positive_tokens.append(self.remove_noise(token, stop_words))

        for token in negative_tokens_dirty:
            negative_tokens.append(self.remove_noise(token, stop_words))

        for article in positive_tokens:
            for token in article:
                if token not in self.sen_dict:
                    self.sen_dict[token] = 1
                else:
                    self.sen_dict[token] += 1

        for article in negative_tokens:
            for token in article:
                if token not in self.sen_dict:
                    self.sen_dict[token] = -1
                else:
                    self.sen_dict[token] -= 1

        positive_model = [(x, 'positive') for x in self.get_dict_model(positive_tokens)]
        negative_model = [(x, 'negative') for x in self.get_dict_model(negative_tokens)]

        dataset2 = positive_model + negative_model
        random.shuffle(dataset2)

        dataset = self.createDatasetFromDict(self.sen_dict)

        print("Training time!!")
        print('length: ', len(dataset))

        self.black_box = NaiveBayesClassifier.train(dataset)

        self.x = classify.accuracy(self.black_box, dataset2[-1000:])
        print("Train accuracy:", classify.accuracy(self.black_box, dataset[:1000]))
        print("Test accuracy:", self.x)

    def createDatasetFromDict(self, sen_dict):
        pos_l, neg_l = [], []
        
        for key, value in sen_dict.items():
            if value < 0:
                for i in range(-value):
                    neg_l.append(key)
            elif value > 0:
                for i in range(value):
                    pos_l.append(key)
            
        random.shuffle(neg_l)
        random.shuffle(pos_l)

        pos_data = [(x, 'positive') for x in self.get_dict_model( [ x for x in self.chunks(pos_l, self.sent_size)] )]
        neg_data = [(x, 'negative') for x in self.get_dict_model( [ x for x in self.chunks(neg_l, self.sent_size)] )]

        data = pos_data + neg_data
        random.shuffle(data)
        return data

    def chunks(self, l, n):
        for i in range(0, len(l), n):
            yield l[i:i+n]

    def lemmatize_text(self, tokens):
        lemmatizer = WordNetLemmatizer()
        lemmatized_sentence = []
        for word, tag in pos_tag(tokens):
            if tag.startswith('NN'):
                pos = 'n'
            elif tag.startswith('VB'):
               pos = 'v'
            else:
               pos = 'a'
            lemmatized_sentence.append(lemmatizer.lemmatize(word, pos))
        return lemmatized_sentence

    def remove_noise(self, tweet_tokens, stop_words = ()):

        cleaned_tokens = []
        for token, tag in pos_tag(tweet_tokens):
            token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'\
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+','', token)

            if tag.startswith("NN"):
                pos = 'n'
            elif tag.startswith('VB'):
                pos = 'v'
            else:
                pos = 'a'

            lemmatizer = WordNetLemmatizer()
            token = lemmatizer.lemmatize(token, pos)

            if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
                cleaned_tokens.append(token.lower())
        return cleaned_tokens

    def loadCorpus(self, file):
        s = 5000
        pos_art = []
        neg_art = []
        line = " "
        i = 0
        while line != "" and i < s:
            line = file.readline().replace("<br />", "")
            part = line.split(",")
            i += 1
            if part[-1] == "positive\n":
                pos_art.append(line[0:len(line)-9])
            elif part[-1] == "negative\n":
                neg_art.append(line[0:len(line)-9])
        if i >= s:
            print("well-well")
        return (pos_art, neg_art)
    
    def get_dict_model(self, cleaned_tokens_list):
        for tweet_tokens in cleaned_tokens_list:
            yield dict([token, True] for token in tweet_tokens)

    def classify(self, s):
        tokens = self.remove_noise(nltk.tokenize.casual_tokenize(s))
        return  self.black_box.classify(dict([token, True] for token in tokens))

    def show_features(self):
        print(self.black_box.show_most_informative_features(50))

def evaluate():
    res = sentiWordNet.classify(text.get("1.0", END))
    text.insert(END, '\n' + res)

if __name__== "__main__":
    
    sentiWordNet = SentimentWordNet()

    pl_good = 'Watchmen remains magnetic in the moment of watching it. The production values are sky-high, and the score by Atticus Ross and Trent Reznor is divine.'
    print("Critic_g:", sentiWordNet.classify(pl_good))

    # sentiWordNet.show_features()

    # print('italian:', sentiWordNet.sen_dict['italian'])

    root = Tk()
    root.title('SentimentwordNet')

    text = Text(root, height=26, width=50)
    text.pack()
    w = Button(root, text='Evaluate', command=evaluate )
    w.pack()
    root.mainloop()
